package it.uniba.gol;

import static org.junit.Assert.*;

import org.junit.Test;

public class CellTest {

	@Test
	public void cellShouldBeAlive() throws Exception {
		Cell cell = new Cell(0, 0, true);
		assertTrue(cell.isAlive());
	}
	
	@Test(expected = CustomLifeException.class)
	public void cellShouldRaiseExceptionOnX() throws Exception {
		new Cell(-1, 0, true);
	}
	
	@Test(expected = CustomLifeException.class)
	public void cellShouldRaiseExceptionOnY() throws Exception {
		new Cell(0, -1, true);
	}
	
	@Test
	public void cellShouldBeDead() throws Exception {
		Cell cell = new Cell(0, 0, true);
		cell.setAlive(false);
		assertFalse(cell.isAlive());
	}
	
	@Test
	public void cellShouldReturnX() throws Exception {
		Cell cell = new Cell(5, 0, true);
		assertEquals(5, cell.getX());
	}
	
	@Test
	public void cellShouldReturnY() throws Exception {
		Cell cell = new Cell(0, 5, true);
		assertEquals(5, cell.getY());
	}
	
	@Test
	public void cellShouldHaveAliveNeighbords() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, false);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, true);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, true);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertEquals(3, cells[1][1].getNumberOfAliveNeighbords());
	}
	
	@Test
	public void aliveCellWithThreeNeighborsShouldSurvive() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, false);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, true);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, true);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertTrue(cells[1][1].willSurvive());
	}
	
	@Test
	public void aliveCellWithTwoNeighborsShouldSurvive() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, false);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, true);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, false);
		cells[2][1] = new Cell(2, 1, true);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertTrue(cells[1][1].willSurvive());
	}
	
	@Test
	public void aliveCellShouldNotSurvive() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, false);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, false);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, false);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertFalse(cells[1][1].willSurvive());
	}
	
	@Test
	public void deadCellShouldNotSurvive() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, false);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, false);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, false);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, false);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertFalse(cells[1][1].willSurvive());
	}
	
	@Test
	public void aliveCellwithMoreThatThreeNeighborsShouldDie() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, true);
		cells[0][1] = new Cell(0, 1, true);
		cells[0][2] = new Cell(0, 2, true);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, true);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertTrue(cells[1][1].willDie());
	}
	
	@Test
	public void aliveCellwithLessThanTwoNeighborsShouldDie() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, false);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, true);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, false);
		cells[2][1] = new Cell(2, 1, false);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertTrue(cells[1][1].willDie());
	}
	
	@Test
	public void aliveCellShouldNotDie() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, false);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, true);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, true);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertFalse(cells[1][1].willDie());
	}
	
	@Test
	public void deadCellShouldNotDie() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, false);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, false);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, false);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, false);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertFalse(cells[1][1].willDie());
	}
	
	@Test
	public void deadCellWithThreeNeighborsShouldRevive() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, true);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, true);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, false);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, false);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertTrue(cells[1][1].willRevive());
	}
	
	@Test
	public void deadCellWithoutThreeNeighborsShouldNotRevive() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, false);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, true);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, false);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, false);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertFalse(cells[1][1].willRevive());
	}
	
	@Test
	public void aliveCellShouldNotRevive() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, false);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, false);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, false);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertFalse(cells[1][1].willRevive());
	}
	
	@Test
	public void cellShouldNotBeNeighborOnX() throws Exception {
		Cell cell1 = new Cell(0, 0, false);
		Cell cell2 = new Cell(2, 2, false);
		
		assertFalse(cell1.isNeighboard(cell2));
	}
	
	@Test
	public void cellShouldNotBeNeighborOnY() throws Exception {
		Cell cell1 = new Cell(0, 0, false);
		Cell cell2 = new Cell(0, 2, false);
		
		assertFalse(cell1.isNeighboard(cell2));
	}
}
